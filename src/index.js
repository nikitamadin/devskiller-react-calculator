import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './components/Layout';
import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(<Layout />, document.getElementById('root'));
