import React from 'react';

export default props => {
  return (
    <div className="card panel-default">
      <div className="card-header">Result</div>

      <p className="card-body">{props.content}</p>
    </div>
  );
}
