import React from 'react';
import Paragraph from './Paragraph';
import Calculation from '../logic/calculation';

export default class Layout extends React.Component {
  state = {
    content: '',
    value: ''
  };

  handleChange = event => {
    this.setState({ value: event.target.value })
  }

  handleSubmit = event => {
    event.preventDefault();

    let calculation = new Calculation(this.state.value);
    let result = calculation.calculate();

    let content = 'Wrong input!';

    if (result !== false) {
      this.setState({ content: result });
    } else {
      this.setState({ content: content });
    }
  }

  render() {
    return (
      <div className="container">
        <section className="jumbotron text-center">
          <h1 className="jumbotron-heading">DevSkiller React calculator</h1>

          <form className="text-center row justify-content-lg-center" onSubmit={this.handleSubmit}>
            <div className="form-group col-lg-4">
              <input
                type="text"
                className="form-control"
                placeholder="expression..."
                autoFocus
                onChange={this.handleChange} />
            </div>

            <div className="form-group col-lg-12">
              <button className="btn btn-primary" type="submit">Submit</button>
            </div>
          </form>
        </section>

        <Paragraph content={this.state.content} />
      </div>
    )
  };
}
