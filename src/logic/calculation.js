export default class Calculation {
  constructor(expression) {
    this.expression = expression;
    this.a = 0;
    this.b = 0;
  }

  addition() {
    return this.a + this.b;
  }

  subtraction() {
    return this.a - this.b;
  }

  production() {
    return this.a * this.b;
  }

  division() {
    return this.a / this.b;
  }

  calculate() {
    let pattern = /(\d+(\.\d+)?)+[*+/-]+(\d+(\.\d+)?)/g;

    if (this.expression.match(pattern)) {
      let matches = pattern.exec(this.expression);
      this.a = parseFloat(matches[1], 10);
      this.b = parseFloat(matches[3], 10);
      let sign = (this.expression.match(/[*+/-]/g)).toString();
      let result = false;

      switch (sign) {
        case '+':
          result = this.addition();
          break;
        case '-':
          result = this.subtraction();
          break;
        case '*':
          result = this.production();
          break;
        case '/':
          result = this.division();
          break;
        default:
          result = false;
          break;
      }

      return result;
    }
    else {
      return false;
    }
  }
}
